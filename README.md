gulp-js-inline
==============

Gulp plutin to inline text files in javascript. This is useful for deploying
resources that should be available to javascript such as templates.

Usage
-----

Input:

    var gulp = require('gulp');
    var concat = require('gulp-concat');
    var inline = require('gulp-js-inline

    var DESTDIR = 'dist';

    gulp.task('templates', function () {
      gulp.src(['templates/*.html'])
        .pipe(inline({
          strip: 1
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(DESTDIR));
    });

Output:

    +(function(store){store['hero.html']='<div>...</div>';})(window._templates=window._templates||{});
    +(function(store){store['info.html']='<div>...</div>';})(window._templates=window._templates||{});

Options
-------

- `name`: variable name used to store resources (defaults to
  `window._templates`)
- `relative`: store resources identified by their name relative to the current
  working directory (defaults to `true`)
- `strip`: strip `strip` levels; similar to `patch -pnum` (defaults to `0`)
