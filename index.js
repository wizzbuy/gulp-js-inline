var path = require('path');
var through = require('through2');
var gutil = require('gulp-util');
var quote = require('js-string-escape');

var PluginError = gutil.PluginError;

module.exports = function (options) {
  options = options || {};

  if (typeof options.name === 'undefined') {
    options.name = 'window._templates';
  }

  if (typeof options.relative === 'undefined') {
    options.relative = true;
  }

  if (typeof options.strip === 'undefined') {
    options.strip = 0;
  }

  if (options.relative) {
    if (typeof options.relative !== 'string') {
      options.relative = process.cwd();
    }
  }

  var transform = function (file, enc, cb) {
    if (file.isNull()) {
      return cb(null, file);
    }

    if (file.isStream()) {
      var err = new PluginError('gulp-js-inline', 'Streaming not supported');
      return cb(err);
    }

    var name = options.relative ? path.relative(options.relative, file.path)
      : file.path;

    if (options.strip) {
      name = name
        .split(path.sep)
        .slice(options.strip)
        .join(path.sep);
    }

    // Transform file's content.
    var contents = quote(file.contents.toString('utf8'));

    // Wrap file's content in javascript.
    contents = [
      '+(function(store){',
        'store[\'' + name + '\']=\'' + contents + '\';',
      '})(' + options.name + '=' + options.name + '||{});'
    ].join('');

    // Update file's content
    file.contents = new Buffer(contents);

    // Continue.
    cb(null, file);
  };

  return through.obj(transform);
};
